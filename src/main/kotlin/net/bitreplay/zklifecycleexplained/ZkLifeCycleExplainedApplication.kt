package net.bitreplay.zklifecycleexplained

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class ZkLifeCycleExplainedApplication

fun main(args: Array<String>) {
    SpringApplication.run(ZkLifeCycleExplainedApplication::class.java, *args)
}
