package net.bitreplay.zklifecycleexplained

object TraceCounter {
    var n: Int = 0;
}

fun t(s: String) {
    println("step ${++TraceCounter.n}: $s")
}