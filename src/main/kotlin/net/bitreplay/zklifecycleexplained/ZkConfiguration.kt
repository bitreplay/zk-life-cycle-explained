package net.bitreplay.zklifecycleexplained

import org.springframework.boot.web.servlet.ServletRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.zkoss.zk.au.http.DHtmlUpdateServlet
import org.zkoss.zk.ui.http.DHtmlLayoutServlet
import org.zkoss.zk.ui.http.HttpSessionListener

@Configuration
class ZkConfiguration {
    @Bean
    fun dHtmlLayoutServlet(): ServletRegistrationBean {
        val params = mapOf("update-uri" to "/zkau")
        val reg = ServletRegistrationBean(DHtmlLayoutServlet(), "*.zul")
        reg.setLoadOnStartup(1)
        reg.initParameters = params
        return reg
    }

    @Bean
    fun dHtmlUpdateServlet(): ServletRegistrationBean {
        val params = mapOf("update-uri" to "/zkau/*")
        val reg = ServletRegistrationBean(DHtmlUpdateServlet(), "/zkau/*")
        reg.setLoadOnStartup(2)
        reg.initParameters = params
        return reg
    }

    @Bean
    fun httpSessionListener(): HttpSessionListener {
        return HttpSessionListener()
    }
}
