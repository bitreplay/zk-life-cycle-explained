package net.bitreplay.zklifecycleexplained.vm

import net.bitreplay.zklifecycleexplained.t
import org.zkoss.bind.annotation.AfterCompose
import org.zkoss.bind.annotation.Init

open class A {
    var abc: String? = null
        get() {
            t(" get abc in class A return $field")
            return field
        }
        set(value) {
            t("set abc to '$value' in class A")
            field = value
        }

    @Init
    private fun init() {
        t("init() in class A")
    }

    @AfterCompose
    private fun afterCompose() {
        t("afterCompose() in class A")
    }
}