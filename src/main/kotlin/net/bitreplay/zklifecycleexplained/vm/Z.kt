package net.bitreplay.zklifecycleexplained.vm

import net.bitreplay.zklifecycleexplained.t
import org.zkoss.bind.annotation.AfterCompose
import org.zkoss.bind.annotation.Init

class Z : A() {
    var xyz: String? = null
        get() {
            t(" get xyz in class Z return $field")
            return field
        }
        set(value) {
            t("set xyz to '$value' in class Z")
            field = value
        }

    @Init(superclass = true)
    private fun init() {
        t("init() in class Z")
    }

    @AfterCompose(superclass = true)
    private fun afterCompose() {
        t("afterCompose() in class Z")
    }
}